--------

AGGREGATOR2.MODULE and AGGREGATOR2_LOGO.MODULE - INSTALLATION -- PLEASE READ:

--------

In order to properly install aggregator2 you should configure taxonomy and setup at least
one vocabulary with terms specific to aggregator2 feeds and items.
This is not essential but recommended or handling the nodes created by aggregator2 feeds
can become difficult if there are many feeds.

It is not essential to understand PHP, Drupal themeing, MySql or the core Drupal modules ECT.
Again, fully understanding these things is not essential to operation of aggregator2 or
Drupal for that matter.  One can operate a boat without knowledge of Archimedes principal
and one can use aggregator2 and Drupal without integral knowledge of PHP.
A boat floats and Drupal works and if you don't care why don't worry about it.
However if you want to modify your boat or extend Drupal that's different. 

The following document attempts to make the installation process simple and it is simple.
Configuration might seem slightly complex at first. It really isn't and you can't make huge
mistakes so remain calm.

Please follow these steps carefully and exactly as written before filing a Support request.
It is advised that you review this entire document before You try and install this module.

-----------

Preparation:

-----------

a.) Setup a Vocabulary and Specify some terms to associate with Feeds and Feed-items.
  You can setup separate terms/vocabularies for aggregator2-feeds and for aggregator2-items.
  But it's good to have one vocabulary for both, and create term for each feed - that will help
  You quickly find all items from given feed.

------------

Installation:

------------

1) The first thing to do is to update your database, adding the 'aggregator2_feed`
   and aggregator2_item` tables.
   This can easily be done from the command line by copying the included
   'aggregator2.mysql' file to your webserver, then running a command something
   like:

      $ mysql -u<username> -p<password> <database> < aggregator2.mysql

   For example, if your username is 'Drupal', your password is 'secret', and
   Your database is called 'Drupal'; you'd type the following command:

      $ mysql -udrupal -psecret drupal < aggregator2.mysql

OR

   You can simply run the scripts provided -- "aggregator2.mysql" in a tool for managing MySQL.
   The DBA module may be installed from the modules directory of Drupal.org without
   having to have command line access scripts window or in PhpMyAdmin

2) Upload 'aggregator2.module' into your modules/ directory.

2.5) Upload "aggregator2_logo.module' into your modules/ directory.
   NOTE this is optional but highly recommended. Even if you don't want to use the functionality
   right away go ahead and drop it in the modules directory with the main module.
   NOTE. The aggregator2_logo.module provides functionality to allow you to associate
   images or logos with specific feeds that are passed to the feed items as nodes are created.
   The effect is to in essence "brand" each feed-item.

3) Now you need to log in to your site and enable the new aggrregator2.module and
   aggregator2_logo.module.
   (Go to: administer -> modules: then check 'aggregator2 and aggregator2_logo')

4. Go to the 'access control' and enable the permissions to suit.
   NOTE you need to enable the "access feed items' for all users or there will be weirdness.

5. Go to settings for aggregator2 and aggregator2 logo and make adjustments to suit.
   The URLs are:

     admin/settings/aggregator2

     admin/settings/aggregator2_logo

   "Create drupal blocks for each feed":
      is the same block creation functionality as aggregator.
      If you want blocks on the sides be sure to check this and then you can
      enable the different blocks in the block admin section.

   "Number of feeds to update at a time:"
      This is for sites with allot of feeds, allot of traffic,
      allot of something that can't handle long cron runs.
      The recommended setting is "ALL" unless you run into problems.

On the aggregator2_logo settings page you will find two check boxes and they are self explanatory.

"Add image to feed description"
"Add image to feed items description"

-------------

Configuration:

-------------

NODE CREATION AND DISPLAY BEHAVIOR:

6) Configure the aggregator2 module for publishing the content the way that suits.
   You have a number of options at your disposal.
   Think about how Slashdot works for a moment to understand what we mean in this section. 

   The Slashdot publishing process or workflow is roughly:
   _>> user submits teaser and article link -->>
      Editorial staff read the submissions and chooses which ones are elevated
      to the front page and when that elevation occurs.
   The entire system is quite nice and very functional however it is manual and
   requires a full time staff.

   Aggregator2 allows you to configure roughly the same thing only without the full time staff.
   The process is:

     1. Subscribe to feeds
     2. Choose which feeds and how many items are elevated from each feed to the front page
        on each cron.

   Allot effort was put into making it flexible.
   You may want to only allow a certain amount of the nodes created by a feed
   to elevate to the front page so that there is a mix of content from various feeds
   on the first page.
   Or you may want certain feeds to create items that NEVER elevate to the front.


Settings:

  URL: This is the feed link or url of the Atom or RSS feed

  Image link: A link to the logo or image you want associated. It can be on or off site.

  Update interval: How many hours in between cron to you want before this feed will once again
     check for new items.

  Discard feed items older than: The time feed items should be kept.
     Older items will be automatically discarded.

  Discard only items not published currently

  Apply changes to already existing items: Checkbox that will only be shown after items
     have been created and is not visible on first run. If set, changes to categories will be
     applied also to already existing items.

  Select categories to be associated with aggregated items

  Update existing items: If enabled, aggragator2 will update already existing items,
    overwriting any changes done between cron runs.

  Publish new items: If enabled, aggragator2 will mark each new item as published.

  Promote items: Select how many of aggregated items should be promoted to front page.
    When new items are created old ones will be taken out from front page.

  Item date source: Select which date will be used for aggregated items.
    If "Feed" is selected, Aggregator2 module will try to find date in feed, and if not found,
    current date will be used. If "Current" is selected, date of creation of items will always
    be set to the one at which items are aggregated.

  Show "full article"/"source site" link: Select place(s) where link to full article
    (for news items) or source site (for news feeds) will be shown.

  Description: this field will keep description of feed which will be found in aggregated data.
    If You want to put own description do that here, aggregator2 will not override Your changes.

-------------

Hints:

-------------

Aggregator2 has several optional settings that control the behavior of the feeds during
the initial update which is when the first items are created from the main feed.
Some of these settings control the behavior on subsequent updates that are performed
during cron runs according to your settings such as update interval and the total number of
feeds you set to be updated per cron run which is set on the aggregator2 settings page
(admin/settings/aggregator2).

Among the settings that control this behavior are
"Update interval",
"Discard feed items older than...",
"Discard only items not published [radio box]",
"Update existing items",
"Publish new items",
"Item date source"

If "Update Existing Items" set incorrectly it can affect the performance of your installation
and rarely your entire server which may raise the ire of your ISP or system admin.
Please understand what takes place during cron.
For each feed that you have checked "Update Existing Items" aggregator2 checks to see
if in fact all the old, already created items have been updated.
If they have been it will update accordingly.
This is only necessary in rare circumstances -- e.g. event listings that are subject
to change or blogs that use natural paths based on titles or other things that are updated often.
Note that most blogs don't update the titles of the posts even if they on rare occasions
do update the post itself.  So for the most part you can safely leave this setting in the default,
unchecked, state.

So if you have allot of feed and have for whatever reason checked the "update the existing items"
radio box you may experience a small to severe performance degradation at cron runs
depending on server speed, number of feeds ect.. the usual stuff.

So leave it unchecked if you are unsure.
